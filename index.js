	
	var express = require("express");
	var app = express();
	var port = 3700;

	var firstrun = 1;

	app.set('views', __dirname + '/tpl');
	app.set('view engine', "jade");
	app.engine('jade', require('jade').__express);
	app.get("/", function(req, res){ res.render("page"); });
	app.use(express.static(__dirname + '/public'));

	//MAIN
	var io = require('socket.io').listen(app.listen(port));

	//Connection between the server and client
	io.sockets.on('connection', function (socket){

		//Sending welcome message
		if(firstrun)
		socket.emit('message', { firstrun: 1 }); 
		
		//Receiving data from the client
		socket.on('send', function (data){
			console.log("#  Changing firstrun");
			firstrun = data.firstrun;
			}); 
	});

	console.log("#  Listering to port " + port);